import React, { useEffect, useState } from 'react';
import { Context } from './helpers/Context';
import './App.css';

import { Routes, Route, useNavigate } from 'react-router-dom';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CourseForm/CourseForm';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';

import RequireAuth from './hoc/RequireAuth';
import AuthProvider from './hoc/AuthProvider';
import PrivateRouter from './components/PrivateRouter/PrivateRouter';

function App() {
	let [searchValue, setSearchValue] = useState('');
	let navigate = useNavigate();
	const userFromLocal = JSON.parse(localStorage.getItem('user'));

	useEffect(() => {
		if (!!userFromLocal) {
			navigate('/courses');
		} else {
			navigate('/login');
		}
	}, []);

	function searchCourses(value) {
		setSearchValue((searchValue = value));
	}
	return (
		<AuthProvider>
			<Context.Provider value={{ searchCourses }}>
				<Routes>
					<Route path='/' element={<Header />}>
						<Route
							path='courses'
							element={
								<RequireAuth>
									<Courses searchValue={searchValue} />
								</RequireAuth>
							}
						/>
						<Route
							path='courses/add'
							element={
								<PrivateRouter>
									<CreateCourse />
								</PrivateRouter>
							}
						/>
						<Route
							path='courses/:id'
							element={
								<RequireAuth>
									<CourseInfo />
								</RequireAuth>
							}
						/>
						<Route
							path='courses/edit/:id'
							element={
								<PrivateRouter>
									<CreateCourse />
								</PrivateRouter>
							}
						/>
						<Route path='registration' element={<Registration />} />
						<Route path='login' element={<Login />} />
					</Route>
				</Routes>
			</Context.Provider>
		</AuthProvider>
	);
}

export default App;
