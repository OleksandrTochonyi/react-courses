export default function pipeDuration(mins) {
	let hours = Math.trunc(mins / 60);
	let minutes = mins % 60;
	if (minutes < 10) {
		return hours + ': 0' + minutes + ' hours';
	} else {
		return hours + ':' + minutes + ' hours';
	}
}
