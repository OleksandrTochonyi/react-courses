export default class CoursesConstructor {
	constructor(title, description, authors, duration) {
		this.title = title;
		this.description = description;
		this.authors = authors;
		this.duration = duration;
	}
}
