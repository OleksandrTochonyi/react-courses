import axios from 'axios';
import { getCourses } from './store/courses/actionCreators';
import { getAuthors } from './store/authors/actionCreators';
import { DB_URL } from './constants';

export async function fetchCourses(dispatch, getState) {
	try {
		const response = await axios.get(DB_URL + '/courses/all');
		const courses = response.data.result;
		dispatch(getCourses(courses));
	} catch (e) {
		console.log('Fetching courses Error=> ' + e);
	}
}

export async function fetchAuthors(dispatch, getState) {
	try {
		const response = await axios.get(DB_URL + '/authors/all');
		const authors = response.data.result;
		dispatch(getAuthors(authors));
	} catch (e) {
		console.log('Fetching authors Error=> ' + e);
	}
}
