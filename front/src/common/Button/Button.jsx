import React from 'react';
import './Button.css';
import PropTypes from 'prop-types';

const Button = ({ text, onClick, buttonClass, testid }) => (
	<button
		data-testid={testid}
		className={buttonClass + ' btn'}
		onClick={onClick}
	>
		{text}
	</button>
);

Button.propTypes = {
	text: PropTypes.string,
	buttonClass: PropTypes.string,
	onClick: PropTypes.func,
};

export default Button;
