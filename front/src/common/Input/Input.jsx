import React from 'react';
import './Input.css';
import PropTypes from 'prop-types';

function Input({
	labelText,
	placeholdetText,
	onChange,
	inputClassName,
	value,
	inputType,
	inputName,
}) {
	return (
		<label>
			{labelText}
			<input
				name={inputName}
				type={inputType ? inputType : 'text'}
				className={inputClassName}
				placeholder={placeholdetText}
				onChange={onChange}
				value={value}
			/>
		</label>
	);
}

Input.propTypes = {
	labelText: PropTypes.string,
	placeholdetText: PropTypes.string,
	onChange: PropTypes.func,
	inputClassName: PropTypes.string,
	value: PropTypes.any,
	inputType: PropTypes.string,
	inputName: PropTypes.string,
};

export default Input;
