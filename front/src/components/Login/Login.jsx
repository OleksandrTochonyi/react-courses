import React from 'react';
import './login.css';

import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect, useDispatch } from 'react-redux';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';

import { signIn } from '../../store/user/actionCreators';
import { getCurrentUser } from '../../store/user/thunk';
import { DB_URL } from '../../constants';

const Login = () => {
	let navigate = useNavigate();
	const dispatch = useDispatch();

	const loginOnPage = (e) => {
		e.preventDefault();
		const form = e.target;
		const email = form.email.value;
		const password = form.password.value;

		if ((!email, !password)) {
			alert('error');
			return;
		}
		axios
			.post(DB_URL + '/login', {
				email: email,
				password: password,
			})
			.then(function (response) {
				let token = response.data.result;
				let user = { ...response.data.user, ...{ token }, isAuth: true };
				dispatch(signIn(user));
				dispatch(getCurrentUser(token));
				navigate('/courses');
			})
			.catch(function (e) {
				console.log('Login Error=> ' + e);
			});
	};

	return (
		<div className='sing-in'>
			<h1>Login</h1>
			<form className='sing-in-form' onSubmit={loginOnPage}>
				<Input
					inputName={'email'}
					inputClassName={'sing-in-form__input'}
					labelText={'Email'}
					placeholdetText={'Enter Email'}
					inputType={'email'}
				/>
				<Input
					inputName={'password'}
					inputClassName={'sing-in-form__input'}
					labelText={'Password'}
					placeholdetText={'Enter Password'}
					inputType={'password'}
				/>
				<Button text={'Login'} buttonClass={'sing-in-btn'} />
			</form>
			<p>
				If you not have an account you can{' '}
				<Link className='link' to={'/registration'}>
					Registration
				</Link>
			</p>
		</div>
	);
};

Login.propTypes = {
	email: PropTypes.string,
	password: PropTypes.string,
};

export default connect(null, signIn)(Login);
