import React, { useEffect, useState } from 'react';
import './courseForm.css';

import { useNavigate, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect, useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import pipeDuration from '../../helpers/pipeDuration';
import { Author } from '../../helpers/AuthorCostructor';
import CoursesConstructor from '../../helpers/CoursesConstructor';

import { createCourse } from '../../store/courses/actionCreators';
import { createAuthor } from '../../store/authors/actionCreators';
import { getAuthors, getCourses } from '../../store/selectors';

import { saveNewAuthor } from '../../store/authors/thunk';
import { saveNewCourse, editCourse } from '../../store/courses/thunk';

const CreateCourse = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const courses = useSelector(getCourses);
	let authors = useSelector(getAuthors);
	let params = useParams();

	let [authorName, setAuthorName] = useState('');
	let [showAuthors, setShowAuthors] = useState(authors);
	let [addedAuthors, setAddedAuthors] = useState([]);
	let [duration, setDuration] = useState('');
	let [courseTitle, setCourseTitle] = useState('');
	let [courseDescription, setCourseDescription] = useState('');

	useEffect(() => {
		if (!!params.id) {
			let course = courses.find((course) => course.id === params.id);
			setAddedAuthors(
				authors.filter((author) => course.authors.includes(author.id))
			);
			setShowAuthors(
				authors.filter((author) => !course.authors.includes(author.id))
			);
			setDuration(course.duration);
			setCourseTitle(course.title);
			setCourseDescription(course.description);
		}
	}, [params.id, authors, courses]);

	function addAuthor(name) {
		if (name.length > 0) {
			let newAuthor = new Author(name);
			dispatch(saveNewAuthor(newAuthor));
			setShowAuthors(showAuthors.concat([newAuthor]));
		}
	}

	function createNewCourse(mode) {
		let authors = [];

		if (
			!!addedAuthors.length &&
			!!duration &&
			!!courseTitle &&
			!!courseDescription
		) {
			addedAuthors.forEach((author) => authors.push(author.id));
			let course = new CoursesConstructor(
				courseTitle,
				courseDescription,
				authors,
				duration
			);
			if (!mode) {
				dispatch(saveNewCourse(course));
			} else {
				let editedCourse = {
					id: params.id,
					course,
				};
				dispatch(editCourse(editedCourse));
			}

			navigate('/courses');
		} else {
			alert('Заполни все поля!!!');
		}
	}

	return (
		<div className='create-course'>
			{params.id ? (
				<Button
					buttonClass={'create-course-btn'}
					text={'Edit Course'}
					onClick={() => {
						createNewCourse('edit');
					}}
				/>
			) : (
				<Button
					buttonClass={'create-course-btn'}
					text={'Create Course'}
					onClick={() => {
						createNewCourse();
					}}
				/>
			)}
			<Input
				labelText={'Title'}
				inputClassName={'create-course__input'}
				placeholdetText={'Enter title...'}
				value={courseTitle}
				onChange={(e) => setCourseTitle((courseTitle = e.target.value))}
			/>
			<label>Description</label>
			<textarea
				placeholder='Enter description'
				value={courseDescription}
				onChange={(e) =>
					setCourseDescription((courseDescription = e.target.value))
				}
			/>
			<div className='create-course__authors'>
				<div className='create-course__add-authors'>
					<h4>Add Author</h4>
					<Input
						labelText={'Author name'}
						inputClassName={'add-authors__input'}
						placeholdetText={'Enter author name...'}
						value={authorName}
						onChange={(e) => {
							setAuthorName((authorName = e.target.value));
						}}
					/>
					<Button
						text={'Create author'}
						buttonClass={'add-authors__button'}
						onClick={() => {
							addAuthor(authorName);
							setAuthorName((authorName = ''));
						}}
					/>
					<h4>Add Duration</h4>
					<Input
						inputType={'number'}
						labelText={'Duration'}
						inputClassName={'add-duration__input'}
						placeholdetText={'Enter duration'}
						value={duration}
						onChange={(e) => {
							const value = e.target.value.replace(/\D+/g, '');
							setDuration((duration = +value));
						}}
					/>
					<p>
						Duration{' '}
						<span>{duration ? pipeDuration(duration) : '0:0 hours'}</span>
					</p>
				</div>
				<div className='create-course__authors-list'>
					<div className='authors-list'>
						<h4>Authors</h4>
						{showAuthors.map((author) => {
							return (
								<p className='create-course__author-name' key={uuidv4()}>
									{author.name}{' '}
									<Button
										text={'Add author'}
										onClick={() => {
											let id = author.id;
											let authorToAdd = showAuthors.find(
												(author) => author.id === id
											);
											setShowAuthors(
												showAuthors.filter((author) => author.id !== id)
											);
											setAddedAuthors(addedAuthors.concat([authorToAdd]));
										}}
									/>
								</p>
							);
						})}
					</div>
					<div className='course-authors'>
						<h4>Course autors</h4>
						{addedAuthors.length > 0 ? (
							<div className='show-author-list'>
								{addedAuthors.map((author) => {
									return (
										<p className='create-course__author-name' key={uuidv4()}>
											{author.name}{' '}
											<Button
												text={'Remove author'}
												onClick={() => {
													let id = author.id;
													let authorToRemove = addedAuthors.find(
														(author) => author.id === id
													);
													setAddedAuthors(
														addedAuthors.filter((author) => author.id !== id)
													);
													setShowAuthors(showAuthors.concat([authorToRemove]));
												}}
											/>
										</p>
									);
								})}
							</div>
						) : (
							<p>Author list is empty</p>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

CreateCourse.propTypes = {
	authorName: PropTypes.string,
	name: PropTypes.string,
	duration: PropTypes.string,
	id: PropTypes.string,
};

const makeStateToProps = (state) => {
	return {
		authors: state.authors,
	};
};

export default connect(makeStateToProps, { createCourse, createAuthor })(
	CreateCourse
);
