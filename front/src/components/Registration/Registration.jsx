import './registration.css';

import { Link } from 'react-router-dom';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';

const Registration = () => {
	let navigate = useNavigate();

	const confirmRegistration = (e) => {
		e.preventDefault();
		const form = e.target;
		const name = form.name.value;
		const email = form.email.value;
		const password = form.password.value;

		if ((!name, !email, !password)) {
			alert('error');
			return;
		}
		axios
			.post('http://localhost:3000/register', {
				name: name,
				email: email,
				password: password,
			})
			.then(function (response) {
				console.log(response);
				navigate('/login');
			})
			.catch(function (error) {
				console.log(error);
			});
	};

	return (
		<div className='sing-in'>
			<h1>Registration</h1>
			<form className='sing-in-form' onSubmit={confirmRegistration}>
				<Input
					inputName={'name'}
					inputClassName={'sing-in-form__input'}
					labelText={'Name'}
					placeholdetText={'Enter Name'}
				/>
				<Input
					inputName={'email'}
					inputClassName={'sing-in-form__input'}
					labelText={'Email'}
					placeholdetText={'Enter Email'}
					inputType={'email'}
				/>
				<Input
					inputName={'password'}
					inputClassName={'sing-in-form__input'}
					labelText={'Password'}
					placeholdetText={'Enter Password'}
					inputType={'password'}
				/>
				<Button
					inputType={'submit'}
					text={'Registration'}
					buttonClass={'sing-in-btn'}
				/>
			</form>
			<p>
				If you have an account you can{' '}
				<Link className='link' to={'/login'}>
					Login
				</Link>
			</p>
		</div>
	);
};

Registration.propTypes = {
	email: PropTypes.string,
	password: PropTypes.string,
	name: PropTypes.string,
};

export default Registration;
