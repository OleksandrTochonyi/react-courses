import React from 'react';
import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getUser } from '../../store/selectors';

const PrivateRouter = ({ children }) => {
	let user = useSelector(getUser);
	if (user.role === 'admin') {
		return children;
	}
	return <Navigate to={'/courses'} />;
};

export default PrivateRouter;
