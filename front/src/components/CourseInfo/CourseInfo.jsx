import React from 'react';
import './courseInfo.css';

import { useParams, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import dateGenerator from '../../helpers/dateGeneratop';
import pipeDuration from '../../helpers/pipeDuration';
import { getCourses, getAuthors } from '../../store/selectors';

const CourseInfo = () => {
	let courses = useSelector(getCourses);
	let authors = useSelector(getAuthors);
	let params = useParams();
	let course = courses.find((course) => course.id === params.id);

	let filteredAuthors = authors.filter((author) => {
		if (course.authors.includes(author.id)) {
			return author;
		}
		return false;
	});
	const authorsArr = filteredAuthors.map((author) => {
		return author.name;
	});

	return (
		<div className='course-info'>
			<Link className='go-back' to={'/courses'}>
				{'< '}
				Back to courses{' '}
			</Link>
			<h1 className='course-info__title'>{course.title}</h1>
			<div className='course-info__content'>
				<div className='course-info__description'>
					<p>{course.description}</p>
				</div>
				<div className='course-info__info'>
					<p>
						<strong>ID: </strong>
						{course.id}
					</p>
					<p>
						<strong>Duration: </strong>
						{pipeDuration(course.duration)}
					</p>
					<p>
						<strong>Created: </strong> {dateGenerator(course.creationDate)}
					</p>
					<p>
						<strong>Author: </strong> <span>{authorsArr.join(', ')}</span>
					</p>
				</div>
			</div>
		</div>
	);
};

CourseInfo.propTypes = {
	courses: PropTypes.array,
	authors: PropTypes.array,
	title: PropTypes.string,
	description: PropTypes.string,
	duration: PropTypes.string,
	creationDate: PropTypes.string,
	id: PropTypes.string,
};

export default CourseInfo;
