import './Courses.css';

import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import CourseCard from './components/CourseCard/CourseCard';
import Button from '../../common/Button/Button';
import SearchBar from './components/SearchBar/SearchBar';

import { getCourses, getAuthors, getUser } from '../../store/selectors';
import { removeCourse } from '../../store/courses/thunk';

const Courses = ({ searchValue }) => {
	let dispatch = useDispatch();
	const navigate = useNavigate();
	let courses = useSelector(getCourses);
	let authors = useSelector(getAuthors);
	let user = useSelector(getUser);

	function showCourse(id) {
		navigate(`/courses/${id}`);
	}

	let filteredCourses = courses.filter((course) => {
		let id = course.id.toLowerCase();
		let title = course.title.toLowerCase();
		let search = searchValue.toLowerCase();
		if (id.includes(search) || title.includes(search)) {
			return true;
		} else {
			return false;
		}
	});

	return (
		<div className='courses' data-testid='courses'>
			<div className='task-bar'>
				<SearchBar
					onClick={() => {
						console.log('click');
					}}
				/>
				{user.role === 'admin' && (
					<Button
						testid={'Add-new-course'}
						text={'Add Course'}
						onClick={() => {
							navigate('/courses/add');
						}}
					/>
				)}
			</div>
			{filteredCourses.map((course) => {
				return (
					<CourseCard
						userRole={user.role}
						authorsArr={authors.filter((authors) =>
							course.authors.includes(authors.id)
						)}
						title={course.title}
						description={course.description}
						duration={course.duration}
						creationDate={course.creationDate}
						key={course.id}
						id={course.id}
						btnOnClick={showCourse}
						editOnClick={() => {
							navigate('edit/' + course.id);
						}}
						removeOnClick={() => {
							dispatch(removeCourse(course.id));
						}}
					/>
				);
			})}
		</div>
	);
};

Courses.propTypes = {
	title: PropTypes.string,
	description: PropTypes.string,
	duration: PropTypes.string,
	creationDate: PropTypes.string,
	id: PropTypes.string,
};

export default Courses;
