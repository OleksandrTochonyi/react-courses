import React from 'react';
import { render, screen } from '@testing-library/react';
import CourseCard from '../CourseCard';

export const mockedCoursesList = {
	id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
	title: 'JavaScript',
	description: `Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum 
has been the industry's standard dummy text ever since the
1500s, when an unknown 
printer took a galley of type and scrambled it to make a type
specimen book. It has survived 
not only five centuries, but also the leap into electronictypesetting, remaining essentially u
nchanged.`,
	creationDate: '8/3/2021',
	duration: 160,
	authors: [
		'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		'f762978b-61eb-4096-812b-ebde22838167',
	],
};

export const mockedAuthorsList = [
	{
		id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		name: 'Vasiliy Dobkin',
	},
	{
		id: 'f762978b-61eb-4096-812b-ebde22838167',
		name: 'Nicolas Kim',
	},
	{
		id: 'df32994e-b23d-497c-9e4d-84e4dc02882f',
		name: 'Anna Sidorenko',
	},
	{
		id: '095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		name: 'Valentina Larina',
	},
];

describe('Course Card testing', () => {
	it('Show course card title', () => {
		render(
			<CourseCard
				title={mockedCoursesList.title}
				description={mockedCoursesList.description}
				duration={mockedCoursesList.duration}
				creationDate={mockedCoursesList.creationDate}
				authors={mockedCoursesList.authors}
				authorsArr={mockedAuthorsList}
			/>
		);
		expect(screen.getByText(/JavaScript/)).toBeInTheDocument();
	});
	it('Show course card description', () => {
		render(
			<CourseCard
				title={mockedCoursesList.title}
				description={mockedCoursesList.description}
				duration={mockedCoursesList.duration}
				creationDate={mockedCoursesList.creationDate}
				authors={mockedCoursesList.authors}
				authorsArr={mockedAuthorsList}
			/>
		);
		expect(screen.getByText(/Lorem Ipsum is simply dummy/)).toBeInTheDocument();
	});
	it('Show course card creation date', () => {
		render(
			<CourseCard
				title={mockedCoursesList.title}
				description={mockedCoursesList.description}
				duration={mockedCoursesList.duration}
				creationDate={mockedCoursesList.creationDate}
				authors={mockedCoursesList.authors}
				authorsArr={mockedAuthorsList}
			/>
		);
		expect(screen.getByText(/8.3.2021/)).toBeInTheDocument();
	});
	it('Show course card duration', () => {
		render(
			<CourseCard
				title={mockedCoursesList.title}
				description={mockedCoursesList.description}
				duration={mockedCoursesList.duration}
				creationDate={mockedCoursesList.creationDate}
				authors={mockedCoursesList.authors}
				authorsArr={mockedAuthorsList}
			/>
		);
		expect(screen.getByText(/2:40 hours/)).toBeInTheDocument();
	});
	it('Show course card authors', () => {
		render(
			<CourseCard
				title={mockedCoursesList.title}
				description={mockedCoursesList.description}
				duration={mockedCoursesList.duration}
				creationDate={mockedCoursesList.creationDate}
				authors={mockedCoursesList.authors}
				authorsArr={mockedAuthorsList}
			/>
		);
		expect(screen.getByText(/Vasiliy Dobkin, Nicolas Kim/)).toBeInTheDocument();
	});
});
