import React from 'react';
import './CourseCard.css';

import PropTypes from 'prop-types';

import dateGenerator from '../../../../helpers/dateGeneratop';
import pipeDuration from '../../../../helpers/pipeDuration';
import Button from '../../../../common/Button/Button';

const CourseCard = (props) => {
	const authors = props.authorsArr.map((author) => author.name);
	return (
		<div className='card' data-testid='course-card'>
			<div className='card__text-content'>
				<h1 data-testid='course-card-title'>{props.title}</h1>
				<p>{props.description}</p>
			</div>
			<div className='card__info'>
				<p>
					<strong>Author: </strong> <span>{authors.join(', ')}</span>
				</p>
				<p>
					<strong>Duration: </strong>
					<span>{pipeDuration(props.duration)}</span>
				</p>
				<p>
					<strong>Create date: </strong>
					<span>{dateGenerator(props.creationDate)}</span>
				</p>
				<div className='buttons-row'>
					<Button
						buttonClass={'card__btn'}
						text={'Show course'}
						onClick={() => props.btnOnClick(props.id)}
					/>
					{props.userRole === 'admin' && (
						<>
							{' '}
							<Button
								buttonClass={'func-btn icon-edit'}
								onClick={() => props.editOnClick(props.id)}
							/>
							<Button
								buttonClass={'func-btn icon-trash'}
								onClick={() => props.removeOnClick(props.id)}
							/>{' '}
						</>
					)}
				</div>
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	title: PropTypes.string,
	description: PropTypes.string,
	creationDate: PropTypes.string,
	id: PropTypes.string,
};

export default CourseCard;
