import React, { useState, useContext } from 'react';
import './SearchBar.css';

import { Context } from '../../../../helpers/Context';
import PropTypes from 'prop-types';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

const SearchBar = () => {
	let [searchText, setSearchText] = useState('');
	const { searchCourses } = useContext(Context);

	function searchValueChange(e) {
		if (e.target.value === '') {
			searchCourses('');
			setSearchText((searchText = ''));
		}
		setSearchText((searchText = e.target.value));
	}

	return (
		<div className='search-form'>
			<Input
				placeholdetText={'Enter courses name or Id...'}
				inputClassName={'search-form__input'}
				value={searchText}
				onChange={(e) => searchValueChange(e)}
			/>
			<Button
				text={'Search'}
				buttonClass={'search-form__button'}
				onClick={() => searchCourses(searchText)}
			/>
		</div>
	);
};

SearchBar.propTypes = {
	searchText: PropTypes.string,
};

export default SearchBar;
