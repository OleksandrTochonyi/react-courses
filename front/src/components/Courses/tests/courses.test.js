import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import Courses from '../Courses';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { Context } from '../../../helpers/Context';

const mockedState = {
	user: {
		role: 'admin',
		isAuth: true,
		name: 'User',
	},
	courses: [
		{
			id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
			title: 'JavaScript',
			description: `Lorem Ipsum is simply dummy text of the printing and
	typesetting industry. Lorem Ipsum
	has been the industry's standard dummy text ever since the
	1500s, when an unknown
	printer took a galley of type and scrambled it to make a type
	specimen book. It has survived
	not only five centuries, but also the leap into electronictypesetting, remaining essentially u
	nchanged.`,
			creationDate: '8/3/2021',
			duration: 160,
			authors: [
				'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
				'f762978b-61eb-4096-812b-ebde22838167',
			],
		},
		{
			id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
			title: 'Angular',
			description: `Lorem Ipsum is simply dummy text of the printing and
	typesetting industry. Lorem Ipsum
	has been the industry's standard dummy text ever since the
	1500s, when an unknown
	printer took a galley of type and scrambled it to make a type
	specimen book. It has survived
	not only five centuries, but also the leap into electronictypesetting, remaining essentially u
	nchanged.`,
			creationDate: '8/3/2021',
			duration: 160,
			authors: [
				'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
				'f762978b-61eb-4096-812b-ebde22838167',
			],
		},
	],
	authors: [
		{
			id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
			name: 'Vasiliy Dobkin',
		},
		{
			id: 'f762978b-61eb-4096-812b-ebde22838167',
			name: 'Nicolas Kim',
		},
		{
			id: 'df32994e-b23d-497c-9e4d-84e4dc02882f',
			name: 'Anna Sidorenko',
		},
		{
			id: '095a1817-d45b-4ed7-9cf7-b2417bcbf748',
			name: 'Valentina Larina',
		},
	],
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
	searchCourses: jest.fn(),
};

describe('Courses', () => {
	it('Courses', () => {
		const history = createMemoryHistory();
		render(
			<Context.Provider value={mockedStore.searchCourses}>
				<Provider store={mockedStore}>
					<Router location={history.location} navigator={history}>
						<Courses searchValue={''} />
					</Router>
				</Provider>
			</Context.Provider>
		);

		screen.debug();

		if (mockedState.courses.length === 0) {
			expect(screen.queryAllByTestId('course-card').length).toBe(
				mockedState.courses.length
			);
		} else {
			expect(screen.queryAllByTestId('course-card').length).toBe(
				mockedState.courses.length
			);
		}
	});
});
