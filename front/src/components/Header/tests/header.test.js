import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import Header from '../Header';

const mockedState = {
	user: {
		isAuth: true,
		name: 'User',
	},
	courses: [],
	authors: [],
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

describe('Header component', () => {
	it('User name & Logo in the doc.', () => {
		render(
			<Provider store={mockedStore}>
				<Header />
			</Provider>
		);
		expect(screen.getByText(/User/)).toBeInTheDocument();
		expect(screen.getByRole('img')).toBeInTheDocument();
	});
});
