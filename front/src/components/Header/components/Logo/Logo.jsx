import React from 'react';
import logo from '../../../../assets/images/epam-logo-primary.png';

const Logo = () => <img alt='Main logo' src={logo} />;

export default Logo;
