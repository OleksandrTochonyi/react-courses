import React from 'react';
import './header.css';

import { Outlet } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect, useDispatch, useSelector } from 'react-redux';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import { getUser, signOut } from '../../store/user/actionCreators';

const Header = () => {
	let dispatch = useDispatch();
	let user = useSelector(getUser);

	return (
		<div className='wrapper'>
			<div className='header'>
				<Logo />
				{user.isAuth && (
					<div className='header__user-info'>
						<p>{user.name}</p>
						<Button
							text={'Logout'}
							onClick={() => {
								dispatch(signOut());
							}}
							buttonClass={'header__button'}
						/>
					</div>
				)}
			</div>
			<Outlet />
		</div>
	);
};

Header.propTypes = {
	name: PropTypes.string,
};

export default connect(null, { signOut })(Header);
