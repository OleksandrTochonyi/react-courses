import React from 'react';
import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getUser } from '../store/selectors';

const RequireAuth = ({ children }) => {
	const userInLocal = localStorage.getItem('user');
	let user = useSelector(getUser);

	if (!user || !userInLocal) {
		return <Navigate to={'/login'} />;
	}
	return children;
};

export default RequireAuth;
