import { createContext } from 'react';

import { useSelector } from 'react-redux';
import { getUser } from '../store/selectors';

export const AuthContext = createContext(null);

const AuthProvider = ({ children }) => {
	let user = useSelector(getUser);
	let value = user;
	return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export default AuthProvider;
