import { combineReducers } from 'redux';
import { coursesReducer } from './courses/reduser';
import { authorsReducer } from './authors/reduser';
import { userReducer } from './user/reduser';
import thunk from 'redux-thunk';
import { compose, createStore, applyMiddleware } from 'redux';

export const rootReducer = combineReducers({
	courses: coursesReducer,
	authors: authorsReducer,
	user: userReducer,
});

export const store = createStore(
	rootReducer,
	compose(
		applyMiddleware(thunk),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
);
