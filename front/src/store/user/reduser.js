import { SIGN_OUT, SIGN_IN, GET_CURRENT_USER } from './actionTypes';

let userInitialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};
export const userReducer = (state = userInitialState, action) => {
	switch (action.type) {
		case GET_CURRENT_USER:
			return { ...action.payload, isAuth: true };
		case SIGN_IN:
			return { ...action.payload };
		case SIGN_OUT:
			delete localStorage['user'];
			return { ...state, isAuth: false };
		default: {
			return state;
		}
	}
};
