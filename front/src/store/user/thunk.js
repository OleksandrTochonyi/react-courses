import axios from 'axios';
import { getCurrentRole } from './actionCreators';
import { DB_URL } from '../../constants';

export function getCurrentUser(token) {
	let userFromLocal = token;
	return async function getUserThunk(dispatch, getState) {
		const headers = { Authorization: userFromLocal };
		try {
			const response = await axios.get(DB_URL + '/users/me', {
				headers,
			});
			const currentUser = { ...response.data.result, token };
			dispatch(getCurrentRole(currentUser));
		} catch (e) {
			console.log('Check current user Error=> ' + e);
		}
	};
}
