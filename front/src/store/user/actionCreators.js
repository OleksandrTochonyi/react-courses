import { GET_CURRENT_USER, SIGN_IN, SIGN_OUT } from './actionTypes';

export function signIn(user) {
	if (!!user.token) {
		localStorage.setItem('user', JSON.stringify(user.token));
	}
	return {
		type: SIGN_IN,
		payload: user,
	};
}

export function getCurrentRole(user) {
	return {
		type: GET_CURRENT_USER,
		payload: user,
	};
}

export function signOut() {
	return {
		type: SIGN_OUT,
	};
}

export const getUser = (state) => state.user;
