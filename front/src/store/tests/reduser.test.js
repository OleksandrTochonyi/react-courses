import { coursesReducer } from '../courses/reduser';
import { createCourse, getCourses } from '../courses/actionCreators';

let newCourse = {
	id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c552ba',
	title: 'JavaScript',
	description: `Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum
has been the industry's standard dummy text ever since the
1500s, when an unknown
printer took a galley of type and scrambled it to make a type
specimen book. It has survived
not only five centuries, but also the leap into electronictypesetting, remaining essentially u
nchanged.`,
	creationDate: '8/3/2021',
	duration: 160,
	authors: [
		'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		'f762978b-61eb-4096-812b-ebde22838167',
	],
};
describe('Reducers tests', () => {
	let action = createCourse(newCourse);
	let fetchAction = getCourses([newCourse]);
	let initialState = [];
	it('Return initial state', () => {
		let newState = coursesReducer(initialState, '');
		expect(newState.length).toBe(0);
	});
	it('Add new course into the store', () => {
		let newState = coursesReducer(initialState, action);
		expect(newState.length).toBe(1);
	});
	it('Fetch course should return new state', () => {
		let newState = coursesReducer(initialState, fetchAction);
		expect(newState.length).toBe(1);
	});
});
