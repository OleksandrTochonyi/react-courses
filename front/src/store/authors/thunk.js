import axios from 'axios';
import { getUserToken } from '../../helpers/getTokenFromState';
import { createAuthor } from './actionCreators';
import { DB_URL } from '../../constants';

export function saveNewAuthor(author) {
	return async function saveNewAuthorThunk(dispatch, getState) {
		const initialAuthor = author;
		const headers = getUserToken(getState());
		try {
			const response = await axios.post(
				DB_URL + '/authors/add',
				initialAuthor,
				{
					headers,
				}
			);
			const newAuthor = response.data.result;
			dispatch(createAuthor(newAuthor));
		} catch (e) {
			console.log('Add new author Error=> ' + e);
		}
	};
}
