import { CREATE_AUTHOR, FETCH_AUTHORS } from './actionTypes';

export function createAuthor(author) {
	return {
		type: CREATE_AUTHOR,
		payload: author,
	};
}

export function getAuthors(authors) {
	return {
		type: FETCH_AUTHORS,
		payload: authors,
	};
}
