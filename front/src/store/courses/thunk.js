import axios from 'axios';
import { getUserToken } from '../../helpers/getTokenFromState';
import { createCourse } from './actionCreators';
import { removeCourse as removeCourseLocal } from './actionCreators';
import { updateCourse } from './actionCreators';
import { DB_URL } from '../../constants';

export function saveNewCourse(course) {
	return async function saveNewTodoThunk(dispatch, getState) {
		const initialCourse = course;
		const headers = getUserToken(getState());
		try {
			const response = await axios.post(
				DB_URL + '/courses/add',
				initialCourse,
				{
					headers,
				}
			);
			let newCourse = response.data.result;
			dispatch(createCourse(newCourse));
		} catch (e) {
			console.log('Add a new course Error=> ' + e);
		}
	};
}

export function removeCourse(id) {
	return async function removeCourseThunk(dispatch, getState) {
		const initialId = id;
		const headers = getUserToken(getState());
		try {
			const response = await axios.delete(DB_URL + `/courses/${initialId}`, {
				headers,
			});
			if (response.status === 200) {
				dispatch(removeCourseLocal(initialId));
			}
		} catch (e) {
			console.log('Remove course Error=> ' + e);
		}
	};
}

export function editCourse(course) {
	return async function editCourseThunk(dispatch, getState) {
		const headers = getUserToken(getState());
		try {
			const response = await axios.put(
				DB_URL + `/courses/${course.id}`,
				course.course,
				{ headers }
			);
			const editedCourse = response.data.result;
			dispatch(updateCourse(editedCourse));
		} catch (e) {
			console.log('Edit course Error=> ' + e);
		}
	};
}
