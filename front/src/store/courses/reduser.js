import {
	CREATE_COURSE,
	FETCH_COURSES,
	REMOVE_COURSE,
	UPDATE_COURSE,
} from './actionTypes';

let coursesInitialState = [];

export const coursesReducer = (state = coursesInitialState, action) => {
	switch (action.type) {
		case CREATE_COURSE:
			return [...state, action.payload];
		case REMOVE_COURSE:
			return state.filter((course) => course.id !== action.payload);
		case UPDATE_COURSE:
			const updatedCourseIndex = state.findIndex(
				(course) => course.id === action.payload.id
			);
			const updatedCourst = {
				...state[updatedCourseIndex],
				...action.payload,
			};
			return [
				...state.slice(0, updatedCourseIndex),
				updatedCourst,
				...state.slice(updatedCourseIndex + 1),
			];
		case FETCH_COURSES:
			return action.payload;
		default: {
			return state;
		}
	}
};
