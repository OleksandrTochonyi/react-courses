import {
	CREATE_COURSE,
	FETCH_COURSES,
	REMOVE_COURSE,
	UPDATE_COURSE,
} from './actionTypes';

export function createCourse(course) {
	return {
		type: CREATE_COURSE,
		payload: course,
	};
}

export function removeCourse(id) {
	return {
		type: REMOVE_COURSE,
		payload: id,
	};
}

export function updateCourse(editedCourse) {
	return {
		type: UPDATE_COURSE,
		payload: editedCourse,
	};
}

export function getCourses(courses) {
	return {
		type: FETCH_COURSES,
		payload: courses,
	};
}
